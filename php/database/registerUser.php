<?php
    include_once('../connection.php');

    header('Content-type: application/json');
    header("Access-Control-Allow-Origin: http://127.0.0.1:5500");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
    header("Access-Control-Allow-Headers: Content-Type");

    $data = json_decode(file_get_contents('php://input'), true);

    if(isset($data['name']) && isset($data['email']) && isset($data['password'])){
        $name = $data['name'];
        $email = $data['email'];
        $password = $data['password'];

        $sql = "SELECT * FROM `estudantes` WHERE `email` = '" . $email . "' OR `nome_estudante` = '" . $name . "'";
        $result = mysqli_query($conn, $sql);

        if(mysqli_num_rows($result) > 0){
            http_response_code(201);
            echo json_encode(array("success" => false, "error" => "Usuário já cadastrado."));
        }else{
            $sql = "INSERT INTO `estudantes` (`nome_estudante`, `email`, `senha`) VALUES ('" . $name . "', '" . $email . "', '" . $password . "')";
            $result = mysqli_query($conn, $sql);

            if($result){
                http_response_code(200);
                echo json_encode(array("success" => true, "message" => "Usuário cadastrado com sucesso."));
            }else{
                http_response_code(201);
                echo json_encode(array("success" => false, "error" => "Erro ao cadastrar usuário."));
            }
        }
    }else{
        http_response_code(699);
        echo json_encode(array("success" => false, "error" => "Dados vazios."));
    }

    mysqli_close($conn);
    exit();
?>