<?php
    include_once('../connection.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");

$data = json_decode(file_get_contents('php://input'), true);



if (isset($data['email']) && isset($data['id'])) {
    $email = $data['email'];

    // iniciando sessão:
    if (!isset($_SESSION)) {
        session_start();
    }

    if($_SESSION['email'] == $email){
        $sql = "SELECT * FROM `estudantes` WHERE `email` = '" . $email . "'";
        $result = mysqli_query($conn, $sql);
    
        if(mysqli_num_rows(mysqli_query($conn, $sql)) > 0){
            $sql = "SELECT * FROM `votantes` WHERE `email` = '" . $email . "'";
    
            $result = mysqli_query($conn, $sql);
    
            if (mysqli_num_rows($result) > 0) {
                http_response_code(201);
                echo json_encode(array("success" => false, "error" => "Usuário já votou."));
            } else {
                $sql = "INSERT INTO `votantes` (`email`) VALUES ('" . $email . "')";
                mysqli_query($conn, $sql);
    
                if ($result) {
                    $id = $data['id'];
    
                    $sql = "UPDATE `candidatos` SET `votos` = `votos` + 1 WHERE `id` = " . $id . ";";
                    $result = mysqli_query($conn, $sql);
    
                    if ($result) {
                        http_response_code(200);
                        echo json_encode(array("success" => true, "message" => "Voto computado com sucesso."));
                    } else {
                        http_response_code(201);
                        echo json_encode(array("success" => false, "error" => "Erro ao computar voto."));
                    }
                } else {
                    http_response_code(201);
                    echo json_encode(array("success" => false, "error" => "Erro ao cadastrar email do votante."));
                }
            }
        }else{
            http_response_code(201);
            echo json_encode(array("success" => false, "error" => "Email não existe no banco de dados."));
        }
    }else{
        http_response_code(201);
        echo json_encode(array("success" => false, "error" => "Email não condiz."));
        exit();
    }

    
    
} else {
    http_response_code(201);
    echo json_encode(array("success" => false, "error" => "Dados vazios."));
}

mysqli_close($conn);
exit();
?>