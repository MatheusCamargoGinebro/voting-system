<?php
    include_once('../connection.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: http://127.0.0.1:5500");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type");

$sql = "SELECT * FROM candidatos";
$results = mysqli_query($conn, $sql);

$index = 0;
while ($record = mysqli_fetch_row($results)) {
    $candidate = array(
        'id' => $record[0],
        'numero_candidato' => $record[1],
        'nome_candidato' => $record[2],
    );
    $candidates[$index] = $candidate;
    $index++;
}

$Data = json_encode($candidates);
echo $Data;

mysqli_close($conn);
exit();
?>