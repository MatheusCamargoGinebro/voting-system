<?php
    if(!isset($_SESSION)){
        session_start();
    }

    echo "Valores da sessão: <br>";
    echo $_SESSION['email']." <br>";
    echo $_SESSION['nome_estudante']." <br>";
    echo $_SESSION['id']." <br>";

    if(isset($_SESSION['email']) && isset($_SESSION['nome_estudante'])){
        echo "<br> Sessão iniciada.";
    }else{
        echo "<br> Sessão não iniciada.";
    }
?>