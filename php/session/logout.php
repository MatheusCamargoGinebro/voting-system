<?php
    // Fechando sessão:
    if(!isset($_SESSION)){
        session_start();
    }
    session_destroy();

    // Enviando resposta:
    http_response_code(200);
    echo json_encode(array("success" => true, "message" => "Sessão fechada com sucesso."));

    exit();
?>