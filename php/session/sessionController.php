<?php
    include_once('../connection.php');

    if(!isset($_SESSION)){
        session_start();
    }

    if(isset($_SESSION['id']) || isset($_SESSION['email'])){
        // retornando o tipo de usuário em json
        if($_SESSION['id'] == 1){
            echo json_encode(array("tipo" => 1, "voted" => false));
        }else{
            // Verificando se o usuário já votou vendo se o email dele existe na tabela votantes
            $sql = "SELECT * FROM `votantes` WHERE `email` = '" . $_SESSION['email'] . "'";
            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) == 1){
                echo json_encode(array("tipo" => 2, "voted" => true));
            }else{
                echo json_encode(array("tipo" => 2, "voted" => false));
            }
        }
    }else{
        echo json_encode(array("tipo" => 0, "voted" => false));
    }
?>