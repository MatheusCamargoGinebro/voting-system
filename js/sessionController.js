// Ao iniciar a página chama o código php que verifica se o usuário está logado:
async function checkSession() {
  const url = "http://localhost:5000/php/session/sessionController.php";
  fetch(url)
    .then((response) => response.json())
    .then((data) => {
        // Verificando em qual página o usuário está:
        const url = window.location.pathname;
        console.log(data);
        console.log("Diretório: ", url);
        
        if(data.tipo == 1){
            if(url == '/html/login.html' || url == '/html/register.html' || url == '/html/voting.html'){
                window.location.href = "http://localhost:5000/html/admin.html";
            }
            console.log("Você é um administrador!");
        } else if (data.tipo == 2){
            if(url == '/html/login.html' || url == '/html/register.html' || url == '/html/admin.html'){
                window.location.href = "http://localhost:5000/html/voting.html";
            } else if (url == '/html/voting.html' && data.voted == true){
                document.getElementById("alert-vote").style.display = "flex";
                document.getElementById("alert-title").innerHTML = "Voto já computado!";
                document.getElementById("alert-text").innerHTML = "Você já havia votado anteriormente! Clique no botão abaixo para voltar e finalizar a sessão.";
            }
            console.log("Você é um eleitor!");
        } else if (data.tipo == 0){
            if(url == '/html/voting.html' || url == '/html/admin.html'){
                window.location.href = "http://localhost:5000/";
            }
            console.log("Você não está logado!");
        }
    });
}

checkSession();