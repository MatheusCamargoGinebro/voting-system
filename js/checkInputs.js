function inputNameChecker(name) {
  var nameRegex = /^[a-zA-Z\s]{3,255}$/;
  return nameRegex.test(name);
}


function inputPasswordChecker(password) {
  var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@#$%^&+=!]).{8,255}$/;
  return passwordRegex.test(password);
}

function passwordConfirmationChecker(password, passwordConfirmation) {
  // Verificando se as senhas são iguais:
  if (password == passwordConfirmation) {
    return true;
  } else {
    return false;
  }
}

function inputEmailChecker(email) {
  // Criando o molde de email padrão:
  var emailRegex = /^[a-zA-Z0-9._-]+@aluno\.ifsp\.edu\.br$/;

  // Verificando se o email é válido:
  if (emailRegex.test(email) && email.length > 0 && email.length < 255) {
    return true;
  } else {
    return false;
  }
}

console.log("checkInputs.js carregado com sucesso!");
