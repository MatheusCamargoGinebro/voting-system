var data;

async function loadCanditates() {
    try {
        const response = await fetch("http://localhost:5000/php/database/getCandidates.php", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        });
    
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        
        data = await response.json();

        var Content = '<option value="">Escolha o seu candidato</option>';

        for (let i = 0; i < data.length; i++){
            Content += '<option value="' + data[i].id + '">' + data[i].nome_candidato + '</option>';
        }

        document.getElementById("voting-candidato").innerHTML = Content;
    
    }catch (error) {
        console.error("Error:", error);
        console.log("OBS: não esquecer de atualizar o conection");
    }
}

loadCanditates();
console.log("loadCanditates.js carregado");