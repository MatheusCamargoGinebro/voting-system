const check = {
  username: false,
  password: false,
};

function checker() {
  if (check.username === true && check.password === true) {
    document.getElementById("login-submit").classList.remove("disabled");
  } else {
    document.getElementById("login-submit").classList.add("disabled");
  }
}

const userName = document
  .getElementById("login-username")
  .addEventListener("input", function (e) {
    if (inputNameChecker(e.target.value) == false) {
      console.log("Você não digitou um nome de usuário!");
      check.username = false;
      document.getElementById("login-username").style.borderColor = "red";
      document.getElementById("login-username-error").style.top = "5px";
    } else {
      console.log("Você digitou um nome de usuário!");
      check.username = true;
      document.getElementById("login-username").style.borderColor = "green";
      document.getElementById("login-username-error").style.top = "-15px";
    }
    checker();
  });

const password = document
  .getElementById("login-password")
  .addEventListener("input", function (e) {
    if (e.target.value == "" || e.target.value.length > 255) {
      console.log("Você não digitou uma senha!");
      check.password = false;
      document.getElementById("login-password").style.borderColor = "red";
      document.getElementById("login-password-error").style.top = "5px";
    } else {
      console.log("Você digitou uma senha!");
      check.password = true;
      document.getElementById("login-password").style.borderColor = "green";
      document.getElementById("login-password-error").style.top = "-15px";
    }
    checker();
  });

const loginSubmit = document
  .getElementById("login-submit")
  .addEventListener("click", function () {
    if (check.username === true && check.password === true) {
      console.log("Dados enviados com sucesso!");

      const name = document.getElementById("login-username").value;
      const password = document.getElementById("login-password").value;

      login(name, password);
    } else {
      console.log("Dados não enviados!");
      checker();

      if (check.username === false) {
        document.getElementById("login-username").style.borderColor = "red";
        document.getElementById("login-username-error").style.top = "5px";
      }

      if (check.password === false) {
        document.getElementById("login-password").style.borderColor = "red";
        document.getElementById("login-password-error").style.top = "5px";
      } 
    }
  });

console.log("processLogin.js carregado com sucesso!");


async function login(name, password) {
  const url = "http://localhost:5000/php/session/login.php";
  const init = {
    method: "POST",
    body: JSON.stringify({
        name: name,
        password: password,
    }),
    headers: {
        "content-type": "application/json",
    },
  };

  try {
    const response = await fetch(url, init);

    const data = await response.json();

    console.log(data);

    if (data.success) {
      window.location.href = "http://localhost:5000/html/voting.html";
    } else {
      console.log("Login não efetuado!");
    }
  }catch(error) {
    console.log("Erro: ", error);
  }
}