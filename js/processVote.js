const check = {
  candidato: false,
  email: false,
};

function checkData() {
  if (check.candidato == true && check.email == true) {
    document.getElementById("voting-submit").classList.remove("disabled");
  } else {
    document.getElementById("voting-submit").classList.add("disabled");
  }
}

// Testando o email:
const email = document
  .getElementById("voting-email")
  .addEventListener("input", function (e) {
    if (!inputEmailChecker(e.target.value)) {
      console.log("Email inválido!");
      check.email = false;

      document.getElementById("voting-email").style.borderColor = "red";
      document.getElementById("voting-email-error").style.top = "5px";
    } else {
      console.log("Email válido!");
      check.email = true;
      
      document.getElementById("voting-email").style.borderColor = "green";
      document.getElementById("voting-email-error").style.top = "-15px";
    }
    checkData();
  });

// Testando se votou em algum candidato:
const candidato = document
  .getElementById("voting-candidato")
  .addEventListener("input", function (e) {
    if (e.target.value === "") {
      console.log("Você não votou em nenhum candidato!");
      check.candidato = false;

      document.getElementById("voting-candidato").style.borderColor = "red";
      document.getElementById("voting-candidato-error").style.top = "5px";
    } else {
      console.log("Você votou em um candidato!");
      check.candidato = true;

      document.getElementById("voting-candidato").style.borderColor = "green";
      document.getElementById("voting-candidato-error").style.top = "-15px";
    }
    checkData();
  });

// Testando o envio dos dados:
const inputButton = document
  .getElementById("voting-submit")
  .addEventListener("click", function () {
    if (check.email === true && check.candidato === true) {
      console.log("Dados enviados com sucesso!");

      // Pegando os dados:
      const email = document.getElementById("voting-email").value;
      const candidato = document.getElementById("voting-candidato").value;

      registerVote(email, candidato);
    } else {
      console.log("Dados não enviados!");
      checkData();

      if(check.email === false){
        document.getElementById("voting-email").style.borderColor = "red";
        document.getElementById("voting-email-error").style.top = "5px";
      }
      if(check.candidato === false){
        document.getElementById("voting-candidato").style.borderColor = "red";
        document.getElementById("voting-candidato-error").style.top = "5px";
      }
    }
  });

async function registerVote(email, id) {
  const url = "http://localhost:5000/php/database/registerVote.php";
  const init = {
    method: "POST",
    body: JSON.stringify({
      email: email,
      id: id,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    const response = await fetch(url, init);
    const data = await response.json();

    console.log(data);

    if(data.success){
      console.log("Voto registrado com sucesso!");
      
      document.getElementById("alert-vote").style.display = "flex";
      document.getElementById("alert-title").innerHTML = "Voto computado com sucesso!";
      document.getElementById("alert-text").innerHTML = "O seu voto foi computado com sucesso! Clique no botão abaixo para voltar e finalizar a sessão.";
    }else{
      console.log("Erro ao registrar voto!");
    }

  } catch (error) {
    console.log("Erro: ", error);
  }
}
