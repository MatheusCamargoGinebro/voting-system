const check = {
    name: false,
    email: false,
    password: false,
    passwordConfirmation: false,
}

const checker = () => {
    if(check.name && check.email && check.password && check.passwordConfirmation) {
        document.getElementById("register-submit").classList.remove("disabled");
    } else {
        document.getElementById("register-submit").classList.add("disabled");
    }
}

const name = document.getElementById("register-name").addEventListener("input", (e) => {
    if(inputNameChecker(e.target.value) == false) {
        console.log("Você não digitou um nome!");
        check.name = false;
        document.getElementById("register-name").style.borderColor = "red";
        document.getElementById("register-name-error").style.top = "5px";
    } else{
        console.log("Você digitou um nome!");
        check.name = true;
        document.getElementById("register-name").style.borderColor = "green";
        document.getElementById("register-name-error").style.top = "-15px";
    }

    checker();
});

const email = document.getElementById("register-email").addEventListener("input", (e) => {
    if(inputEmailChecker(e.target.value) == false) {
        console.log("Você não digitou um email!");
        check.email = false;
        document.getElementById("register-email").style.borderColor = "red";
        document.getElementById("register-email-error").style.top = "5px";
    } else{
        console.log("Você digitou um email!");
        check.email = true;
        document.getElementById("register-email").style.borderColor = "green";
        document.getElementById("register-email-error").style.top = "-15px";
    }

    checker();
});

const password = document.getElementById("register-password").addEventListener("input", (e) => {
    if(inputPasswordChecker(e.target.value) == false) {
        console.log("Você não digitou uma senha!");
        check.password = false;
        document.getElementById("register-password").style.borderColor = "red";
        document.getElementById("register-password-error").style.top = "5px";
    } else{
        console.log("Você digitou uma senha!");
        check.password = true;
        document.getElementById("register-password").style.borderColor = "green";
        document.getElementById("register-password-error").style.top = "-15px";
    }

    if(passwordConfirmationChecker(document.getElementById("register-password-confirmation").value, e.target.value) == false) {
        console.log("Você não digitou uma senha!");
        check.passwordConfirmation = false;
        document.getElementById("register-password-confirmation").style.borderColor = "red";
        document.getElementById("register-password-confirmation-error").style.top = "5px";
    } else{
        console.log("Você digitou uma senha!");
        check.passwordConfirmation = true;
        document.getElementById("register-password-confirmation").style.borderColor = "green";
        document.getElementById("register-password-confirmation-error").style.top = "-15px";
    }

    checker();
});

const passwordConfirmation = document.getElementById("register-password-confirmation").addEventListener("input", (e) => {
    if(passwordConfirmationChecker(document.getElementById("register-password").value, e.target.value) == false) {
        console.log("Você não digitou uma senha!");
        check.passwordConfirmation = false;
        document.getElementById("register-password-confirmation").style.borderColor = "red";
        document.getElementById("register-password-confirmation-error").style.top = "5px";
    } else{
        console.log("Você digitou uma senha!");
        check.passwordConfirmation = true;
        document.getElementById("register-password-confirmation").style.borderColor = "green";
        document.getElementById("register-password-confirmation-error").style.top = "-15px";
    }

    checker();
});

const registerSubmit = document.getElementById("register-submit").addEventListener("click", () => {
    if(check.name === true && check.email === true && check.password === true && check.passwordConfirmation === true) {
        console.log("Dados enviados com sucesso!");

        // Pegando os dados:
        const name = document.getElementById("register-name").value;
        const email = document.getElementById("register-email").value;
        const password = document.getElementById("register-password").value;

        register(name, email, password);
    } else {
        console.log("Dados não enviados!");
        checker();

        if(check.name === false) {
            document.getElementById("register-name").style.borderColor = "red";
            document.getElementById("register-name-error").style.top = "5px";
        }

        if(check.email === false) {
            document.getElementById("register-email").style.borderColor = "red";
            document.getElementById("register-email-error").style.top = "5px";
        }

        if(check.password === false) {
            document.getElementById("register-password").style.borderColor = "red";
            document.getElementById("register-password-error").style.top = "5px";
        }

        if(check.passwordConfirmation === false) {
            document.getElementById("register-password-confirmation").style.borderColor = "red";
            document.getElementById("register-password-confirmation-error").style.top = "5px";
        }
    }
});

async function register(name, email, password) {
    const url = "http://localhost:5000/php/database/registerUser.php";
    const init = {
        method: "POST",
        body: JSON.stringify({
            name: name,
            email: email,
            password: password,
        }),
        headers: {
            "content-type": "application/json",
        },
    };
    try {
        const response = await fetch(url, init);
        const data = await response.json();

        console.log(data);

        if(data.success) {
            console.log("Usuário cadastrado com sucesso!");
            window.location.href = "http://localhost:5000/html/login.html";
        }

    }catch(error) {
        console.log("Erro: ", error);
    }
}